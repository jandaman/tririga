/*jshint esversion: 6 */

function saveOptions(e) {
  browser.storage.local.set({
    settings: {
      marginRight: document.querySelector( "#marginRight" ).value,
      marginTop: document.querySelector( "#marginTop" ).value,
      delay: document.querySelector( "#interval" ).value,
      link: document.querySelector( "#pageLink" ).value
    }
  });
  e.preventDefault();
}

function restoreOptions() {
  browser.storage.local.get( "settings" ).then( ( { settings } ) => {

    document.querySelector( "#marginRight" ).value = settings.marginRight || "0";
    document.querySelector( "#marginTop" ).value = settings.marginTop || "30";
    document.querySelector( "#interval" ).value = settings.delay || "15";
    document.querySelector( "#pageLink" ).value = settings.link || "/html/en/default/admin/monitor.jsp?username=system&action=WF_EVENT_COUNT";
  });
}

document.addEventListener( 'DOMContentLoaded', restoreOptions );
document.querySelector( "form" ).addEventListener( "submit", saveOptions );
