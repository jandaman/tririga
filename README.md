# Tririga Workflow Monitor
A browser plugin specific for use with IBM Tririga.
The plugin allow for displaying the active workflow count directly in the Tririga user front-end.
The plugin rely on open access to the Tririga monitor at /html/en/default/admin/monitor.jsp

The settings page allow to specify the update frequency and to set a different location of the monitors on your environment.
Note, the plugin expect the monitor page to be on the same host as the application front-end.
If the front-end is accessed at https://tri.example.org, the plugin will by default expect the monitor to be located at
https://tri.example.org/html/en/default/admin/monitor.jsp


## License
see [LICENSE](LICENSE) file.
