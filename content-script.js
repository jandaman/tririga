/*jshint esversion: 6 */

let intervl;
let delay = 15;
let marginRight = 0;
let marginTop = 50;
let pathQuery = "/html/en/default/admin/monitor.jsp?username=system&action=WF_EVENT_COUNT";


if ( window.location !== window.parent.location ) {
  /*** buuuuhhhhh this document is the content of an iframe - DAMN you IBM - you lazy s......  ***/
} else {
  let bdy = document.getElementsByTagName('body')[0];
  if ( bdy ) {

    exists = bdy.querySelector("div.tririga-monitor");
    if ( !exists ) {
      /* only inject the monitor html code if it doesn't already exist */

      var d = document.createElement( "div" );
      d.innerHTML = `<div class="row"><label class="count" id="count">0</label></div><div class="row"><progress class="countDown row" id="countDown" max="0", value="0" />`;
      d.classList.add( "tririga-monitor" );
      bdy.insertBefore( d, bdy.firstChild );

      // retrieve the settings from the option form.
      getSettings();
    }

  } else {

    document.body.style.border="5px solid red";
    console.log( "Unable to locate body element" );

  }
}


function getSettings() {
  browser.storage.local.get( "settings" ).then( ( { settings } ) => {
    // for some reason, get returns successly, even though the key is not found
    if ( settings ) {
      pathQuery = settings.link;
      delay = settings.delay;
      marginRight = settings.marginRight;
      marginTop = settings.marginTop;
    }
    applySettings();
  });
}


function applySettings() {
  let d = document.querySelector("div.tririga-monitor");
  if ( d ) {
    d.style.top = `${marginTop}px`;
    d.style['margin-right'] = `${marginRight}px`;
  }
  let p = document.querySelector("progress.countDown");
  if ( p ) {
    p.max = delay;
  }
  getCount();
  setRefreshTimer();
}


browser.runtime.onMessage.addListener( ( message, sender, sendResponse ) => {
    if ( message.action === "die" ) {
      kill();
      return Promise.resolve( { action: "done" } );
    }
    if ( message.action === "live" ) {
      revive();
      return Promise.resolve( { action: "done" } );
    }
    if ( message.action === "ping" ) {
      return Promise.resolve( { action: "pong" } );
    }
});


function revive() {
  let d = document.querySelector("div.tririga-monitor");
  if ( d ) {
    getCount();
    setRefreshTimer();
    d.style.display = "block";
  }
}


function kill() {
  // don't really kill - we are not babaric
  // if we need to revive later, we would get an problem with the already defined script assets
  clearRefreshTimer();

  let d = document.querySelector("div.tririga-monitor");
  if ( d ) {
      d.style.display = "none";
  }
}


function setRefreshTimer() {
  if ( !intervl ) {
    intervl = setInterval( updateCountDown, 1000 );
  }
}


function clearRefreshTimer() {
  if ( intervl ) {
    clearInterval( intervl );
    intervl = false;
  }
}


function updateCountDown() {
  let e = document.querySelector( "progress.countDown" );
  if ( e ) {
    if ( e.value == delay ) {
      e.value = 0;
      getCount();
    } else {
      e.value++;
    }
  }
}


/*** get the workflow count ***/
function getCount() {
  if ( pathQuery ) {
    let url = "https://" + document.location.hostname + pathQuery;
    l = d.querySelector( "div.row label.count" );
    if ( l ) {
      fetch( url )
        .then( response => {
          if( response.status == 200 ) {
            return response.text();
          }
        })
        .then( data => {
          l.innerText = data.match( /\b.+\b/ );
        })
        .catch( err => {
          console.log( "Auch", err );
        });
    }
  }
}
