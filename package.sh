#!/usr/bin/env bash


zip -r -FS ../tririga_workflow_monitor_source_files.zip * --exclude '*.git*' --exclude '*.min.*' --exclude 'documentation/*' --exclude 'package.sh'
zip -r -FS ../tririga_workflow_monitor.zip -r -FS ../tririga_workflow_monitor.zip *.min.* *.html manifest.json README.md LICENSE *.png --exclude 'documentation/*'
