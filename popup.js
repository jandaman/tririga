/*jshint esversion: 6 */

let tabsList = [];


// initialize tab list, and set the enable slider initial state if current tab is in list.
getTabList().then( list => {
  if ( list ) {
    tabsList = list;
    getTabInfo().then( tabInfo => {
      if ( isTabInList( tabInfo ) ) {
        let enablebtn = document.querySelector( "#enableBtn" );
        if ( enableBtn ) {
          enableBtn.checked = true;
        }
        // Send message to content script, that the add-on should be active
        browser.tabs.sendMessage( tabInfo.id, { action: "live"} );
      }
    });
  }
});


let enablebtn = document.querySelector( "#enableBtn" );
if ( enableBtn ) {
  enableBtn.addEventListener( "click", (e) => {
    if ( e.target.checked ) {
      getTabInfo().then( tabInfo => {
        if ( !isTabInList( tabInfo ) ) {
          tabsList.push( tabInfo );
          storeTabsList( tabInfo )
            .then( getTabList() );
        }

        // try and ping the content script - if no response, then load the content script
        browser.tabs.sendMessage( tabInfo.id, { action: "ping" })
        .then ( response => {
          response = response || {};
          if (response.action === "pong") {
            browser.tabs.sendMessage( tabInfo.id, { action: "live"} );
          }
        }).
        catch( error => {
          browser.tabs.executeScript( tabInfo.id, {
            file: "/browser-polyfill.min.js"
          });
          browser.tabs.executeScript( tabInfo.id, {
            file: "/content-script.min.js"
          });
          browser.tabs.insertCSS( tabInfo.id, {
            file: "/content-script.min.css"
          });
        });
      });

    } else {
      getTabInfo().then( tabInfo => {
        removeTabFromList( tabInfo );
        storeTabsList( tabInfo )
          .then( getTabList() );
        browser.tabs.sendMessage( tabInfo.id, { action: "die"} );
      });
    }
  });
}


function onError( error ) {
  console.log( error );
}


function onSuccess( msg ) {
  console.log( msg );
}


function getTabInfo() {
  return browser.tabs.query({currentWindow: true, active: true})
  .then( tabInfo => {
    let tab = {};
    tab.id = tabInfo[0].id;
    tab.index = tabInfo[0].index;
    tab.title = tabInfo[0].title;
    tab.url = tabInfo[0].url;
    return tab;
  });
}


function removeTabFromList( tabInfo ) {
  let i = 0;
  while ( i < tabsList.length ) {
    if ( tabsList[i].id == tabInfo.id ) {
      tabsList.splice( i, 1 );
    } else {
      i++;
    }
  }
}


function isTabInList( tabInfo ) {
  let i = 0;
  while ( i < tabsList.length ) {
    if ( tabsList[i].id == tabInfo.id ) {
      return true;
    }
    i++;
  }
  return false;
}


function storeTabsList( tabInfo ) {
  return browser.storage.local.set( {  tabsList: tabsList } )
    .then( onSuccess( "tabsList stored" ), onError );
}


function getTabList( ) {
  // Get the stored list
  return browser.storage.local.get( "tabsList" )
    .then( data => {
      if ( !data.tabsList ) {
        tabsList = [];
      } else {
        tabsList = data.tabsList;
      }
      return tabsList;
    });
}
